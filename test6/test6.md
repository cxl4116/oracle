# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | 

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 表空间设计方案：

```
-- 创建管理员表空间
CREATE TABLESPACE system_tablespace
 DATAFILE 'system_tablespace.dbf'
 SIZE 100M AUTOEXTEND ON
 NEXT 50M MAXSIZE UNLIMITED
 EXTENT MANAGEMENT LOCAL;

-- 创建用户表空间
CREATE TABLESPACE user_tablespace
 DATAFILE 'user_tablespace.dbf'
 SIZE 100M AUTOEXTEND ON
 NEXT 50M MAXSIZE UNLIMITED
 EXTENT MANAGEMENT LOCAL;

```
![第一步](1_1.png)
![第一步](1_2.png)



## 表设计方案：
```
1.用户表、订单表存储在system_tablespace表空间中，商品表、库存表存储在user_tablespace表空间中。
2.用户表存储系统中所有的用户信息。
商品表存储商品信息。
订单表存储用户下单的订单信息，同时与用户表和商品表建立关系。
库存表存储商品库存信息，同时与商品表建立关系。
```
```
-- 创建用户表
CREATE TABLE users (
user_id NUMBER PRIMARY KEY,
user_name VARCHAR2(50),
password VARCHAR2(50),
email VARCHAR2(50)
) TABLESPACE system_tablespace;

-- 创建商品表
CREATE TABLE products (
product_id NUMBER PRIMARY KEY,
product_name VARCHAR2(50),
product_price NUMBER,
product_desc VARCHAR2(100)
) TABLESPACE user_tablespace;
```
![第二步](2_1.png)
![第二步](2_2.png)
```
-- 创建订单表
CREATE TABLE orders (
order_id NUMBER PRIMARY KEY,
user_id NUMBER,
product_id NUMBER,
order_time DATE,
order_desc VARCHAR2(100),
FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,
FOREIGN KEY (product_id) REFERENCES products(product_id) ON DELETE CASCADE
) TABLESPACE system_tablespace;

-- 创建库存表
CREATE TABLE inventory (
product_id NUMBER,
inventory_qty NUMBER,
warehouse_name VARCHAR2(50),
PRIMARY KEY (product_id, warehouse_name),
FOREIGN KEY (product_id) REFERENCES products(product_id) ON DELETE CASCADE
) TABLESPACE user_tablespace;
```
![第二步](2_3.png)
![第二步](2_4.png)
```
-- 在users表中插入模拟用户数据50000条
INSERT INTO users (user_id, user_name, password, email)
SELECT LEVEL, 'User-' || LEVEL, 'password', 'user' || LEVEL || '@example.com'
FROM dual
CONNECT BY LEVEL <= 50000;
COMMIT;
```
![第二步](2_5.png)
```
-- 在products表中插入模拟商品数据100000条
INSERT INTO products (product_id, product_name, product_price, product_desc)
SELECT LEVEL, 'Product-' || LEVEL, TRUNC(DBMS_RANDOM.VALUE(1, 10000)), 'Description-' || LEVEL
FROM dual
CONNECT BY LEVEL <= 100000;
COMMIT;
```
![第二步](2_6.png)
```
-- 在inventory表中插入模拟库存数据100000条
INSERT INTO inventory (product_id, inventory_qty, warehouse_name)
SELECT TRUNC(DBMS_RANDOM.VALUE(1, 100000)), TRUNC(DBMS_RANDOM.VALUE(1, 100)), 'Warehouse-' || MOD(LEVEL, 10)
FROM dual
CONNECT BY LEVEL <= 100000;
COMMIT;
```
![第二步](2_7.png)
```
-- 在orders表中插入模拟订单数据200000条
INSERT INTO orders (order_id, user_id, product_id, order_time, order_desc)
SELECT LEVEL, TRUNC(DBMS_RANDOM.VALUE(1, 50000)), TRUNC(DBMS_RANDOM.VALUE(1, 100000)), SYSDATE - TRUNC(DBMS_RANDOM.VALUE(0, 100)), 'Order-' || LEVEL
FROM dual
CONNECT BY LEVEL <= 200000;
COMMIT;
```
![第二步](2_8.png)
## 权限及用户分配方案：

```
-- 创建管理员用户admin
CREATE USER admin IDENTIFIED BY admin123 DEFAULT TABLESPACE system_tablespace QUOTA UNLIMITED ON system_tablespace;
GRANT CREATE SESSION TO admin;
GRANT CONNECT TO admin;
GRANT RESOURCE TO admin;

-- 在system_tablespace中创建admin模式
CREATE USER admin_ut IDENTIFIED BY admin123 DEFAULT TABLESPACE system_tablespace QUOTA UNLIMITED ON system_tablespace;
GRANT CREATE SESSION TO admin_ut;
GRANT RESOURCE TO admin_ut;

-- 创建销售账户sales
CREATE USER sales IDENTIFIED BY sales123 DEFAULT TABLESPACE user_tablespace QUOTA UNLIMITED ON user_tablespace;
GRANT CREATE SESSION TO sales;
GRANT CONNECT TO sales;
GRANT RESOURCE TO sales;

-- 在user_tablespace中创建sales模式
CREATE USER sales_ut IDENTIFIED BY sales123 DEFAULT TABLESPACE user_tablespace QUOTA UNLIMITED ON user_tablespace;
GRANT CREATE SESSION TO sales_ut;
GRANT RESOURCE TO sales_ut;
```
![第三步](3_1.png)    
![第三步](3_2.png)                  
![第三步](3_3.png)
![第三步](3_4.png)
```
– 给admin用户授权管理员权限
GRANT DBA TO admin;
– 给sales用户授权查询订单信息及商品信息的权限
GRANT SELECT ON orders TO sales_ut;
GRANT SELECT ON products TO sales_ut;
```
![第三步](3_5.png)
![第三步](3_6.png)

## 存储过程和函数设计：

```
1.add_product：添加新商品信息。
2.delete_product：删除指定商品信息。
3.get_product：查询指定商品ID和名称的商品信息。
4.get_order：查询指定用户和订单ID的订单信息。
5.generate_order：生成订单。
6.get_inventory：查询指定商品和仓库的库存信息。
```
```
-- 添加商品信息add_product
CREATE OR REPLACE PROCEDURE add_product(
p_product_id NUMBER,
p_product_name VARCHAR2,
p_product_price NUMBER,
p_product_desc VARCHAR2
) IS
BEGIN
INSERT INTO products (
product_id,
product_name,
product_price,
product_desc
) VALUES (
p_product_id,
p_product_name,
p_product_price,
p_product_desc
);
END;
/
```
![第四步](4_1.png)
```
-- 删除商品信息delete_product
CREATE OR REPLACE PROCEDURE delete_product(
p_product_id NUMBER
) IS
BEGIN
DELETE FROM products WHERE product_id = p_product_id;
END;
/
```
![第四步](4_2.png)
```
-- 查询商品信息get_product
CREATE OR REPLACE FUNCTION get_product(
p_product_id NUMBER,
p_product_name VARCHAR2
) RETURN products%ROWTYPE IS
v_product products%ROWTYPE;
BEGIN
SELECT *
INTO v_product
FROM products
WHERE product_id = p_product_id AND product_name = p_product_name;
RETURN v_product;
END;
/
```
![第四步](4_3.png)
```
-- 查询订单get_order
CREATE OR REPLACE FUNCTION get_order(
p_user_id NUMBER,
p_order_id NUMBER
) RETURN orders%ROWTYPE IS
v_order orders%ROWTYPE;
BEGIN
SELECT *
INTO v_order
FROM orders
WHERE user_id = p_user_id AND order_id = p_order_id;
RETURN v_order;
END;
/
```
![第四步](4_4.png)
```
-- 生成订单
---创建序列orders_seq
CREATE SEQUENCE orders_seq
START WITH 1
INCREMENT BY 1
NOMAXVALUE
NOCYCLE
NOCACHE;
---创建生成订单generate_order
CREATE OR REPLACE PROCEDURE generate_order(
p_user_id NUMBER,
p_product_id NUMBER,
p_order_desc VARCHAR2
) IS
BEGIN
INSERT INTO orders (
order_id,
user_id,
product_id,
order_time,
order_desc
) VALUES (
orders_seq.NEXTVAL,
p_user_id,
p_product_id,
SYSDATE,
p_order_desc
);
END;
/
```
![第四步](4_5.png)
```
-- 查询库存get_inventory
CREATE OR REPLACE FUNCTION get_inventory(
p_product_id NUMBER,
p_warehouse_name VARCHAR2
) RETURN inventory%ROWTYPE IS
v_inventory inventory%ROWTYPE;
BEGIN
SELECT *
INTO v_inventory
FROM inventory
WHERE product_id = p_product_id AND warehouse_name = p_warehouse_name;
RETURN v_inventory;
END;
/
```
![第四步](4_6.png)

## 备份方案：
```
定期备份是数据库备份方案的重要部分，设置每周自动备份一次数据库。利用Oracle提供的工具实现自动化备份。
-- 编辑定时任务  
0 2 * * 1 sh /path/to/backup.sh  
-- 创建备份脚本
#!/bin/bash
ORACLE_HOME=/oracle/app/oracle/product/11.2.0/dbhome_1
export ORACLE_HOME
ORACLE_SID=orcl
export ORACLE_SID
DATE=$(date +%Y%m%d-%H%M%S)
exp sys/password@orcl file=/home/backup/backup_$DATE.dmp log=/home/backup/backup_$DATE.log full=y
-- 给backup.sh文件添加可执行权限  
chmod u+x backup.sh
```
```
日志备份是数据库备份方案的一部分，通过备份数据库的日志文件，可以保障数据在一定时间范围内的完整性。  
-- 将日志备份设置为归档模式  
ALTER DATABASE ARCHIVELOG;
-- 查看归档日志文件存储路径
SHOW PARAMETER LOG_ARCHIVE_DEST
-- 手动备份日志文件
ALTER SYSTEM ARCHIVE LOG CURRENT;
```

## 总结与体会
```
通过对商品销售系统的表空间设计方案、用户权限分配方案、程序包设计方案和备份方案设计等方面的思考可以看出，设计一个系统需要考虑多个因素和环节。以下是我对此过程的总结和体会：

表空间设计是系统设计中的核心之一，它直接影响到系统的性能和可靠性。在设计表空间时，需要根据系统功能和数据特性进行合理划分，使得数据读写操作更加高效。

权限分配是保障系统安全性的关键因素之一，需要根据用户角色的不同进行明确划分和权限控制。只有建立了严格的安全权限控制策略，才能保证系统的数据安全性。

程序包设计是实现业务逻辑的关键因素之一，需要考虑代码的可维护性、可读性和可扩展性等方面。设计好的程序包可以使得各个模块之间的数据交互更加高效。

备份方案设计是保障系统可靠性的重要手段，备份方案的设计需要考虑备份数据的存储位置和有效性检测等因素。只有所有备份相关流程都经过完备的测试和验证，才能确保备份系统的可靠性。

总之，这些设计方案的制定，需要综合考虑多个因素，才能使系统更加完备可靠。在未来的设计工作中，我将更加注重细节和系统性的整体思考，以便为系统的建设提供更好的保障。
```