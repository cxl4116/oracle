# 实验2：用户及权限管理
- 姓名 陈小龙
- 学号 202010414302
## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

1.在pdborcl插接式数据中创建一个新的本地角色role_cxl，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有role_cxl的用户就同时拥有这三种权限。
2.创建角色之后，再创建用户cxl，给用户分配表空间，设置限额为50M，授予con_res_role角色。
最后测试：用新用户cxl连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

### 创建角色和用户并给用户授权和分配空间

```
SQL> CREATE ROLE role_cxl;

角色已创建。

SQL> GRANT connect,resource,CREATE VIEW TO role_cxl;

授权成功。

SQL> CREATE USER cxl  IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;

用户已创建。


SQL> ALTER USER cxl  default TABLESPACE "USERS";

用户已更改。

SQL> ALTER USER cxl  QUOTA 50M ON users;

用户已更改。


SQL> GRANT role_cxl  TO cxl;

授权成功。
```
![第一步](image1.png)


### 新用户cxl连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。

```
SQL> show user;
USER 为 "CXL"
SQL> SELECT * FROM session_privs;

PRIVILEGE
----------------------------------------
CREATE SESSION
CREATE TABLE
CREATE CLUSTER
CREATE VIEW
CREATE SEQUENCE
CREATE PROCEDURE
CREATE TRIGGER
CREATE TYPE
CREATE OPERATOR
CREATE INDEXTYPE
SET CONTAINER

已选择 11 行。

SQL> SELECT * FROM session_roles;

ROLE
--------------------------------------------------------------------------------
ROLE_CXL
CONNECT
RESOURCE
SODA_APP

CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;

表已创建。

SQL> 
已创建 1 行。

SQL> 
已创建 1 行。

SQL> 
视图已创建。

SQL> 
授权成功。

SQL> SELECT * FROM customers_view;

NAME
--------------------------------------------------
zhang
wang

```

### 用户hr连接到pdborcl，查询cxl授予它的视图customers_view

```
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM cxl.customers_view;

NAME
--------------------------------------------------
zhang
wang

```
![第三步](image3.png)
测试一下用户hr,cxl之间的表的共享，只读共享和读写共享都测试一下。 cxl用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

### 概要文件设置,用户最多登录时最多只能错误3次

```
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user cxl unlock命令解锁。

```
$ sqlplus system/123@pdborcl
SQL> alter user cxl  account unlock;
```
![第四步](image4.png)
### 数据库和表空间占用分析

当实验做完之后，数据库pdborcl中包含了新的角色con_res_role和用户sale。
新用户sale使用默认表空间users存储表的数据。
随着用户往表中插入数据，表空间的磁盘使用量会增加。

查看数据库的使用情况
查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```
![第五步](image5.png)
autoextensible是显示表空间中的数据文件是否自动增加。
MAX_MB是指数据文件的最大容量。

### 实验结束删除用户和角色

```
$ sqlplus system/123@pdborcl
SQL>
drop role role_cxl;
drop user cxl cascade;
```
![第六步](image6.png)
![第六步](image7.png)
