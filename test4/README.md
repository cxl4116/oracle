# 实验4：PL/SQL语言打印杨辉三角

- 学号: 202010414302
- 姓名: 陈小龙
## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null;
i integer;
j integer;
spaces varchar2(30) :='   ';
N integer := 9;
rowArray t_number := t_number();
begin
    dbms_output.put_line('1');
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put_line(''); 
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N
    loop
        rowArray(i):=1;    
        j:=i-1;
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));
        end loop;
        dbms_output.put_line('');
    end loop;
END;
/
```
![第一步](1.png)
## 创建YHTriange存储过程
```sql
CREATE OR REPLACE PROCEDURE YHTriange (N IN INTEGER) AS
  TYPE t_number IS VARRAY(100) OF INTEGER NOT NULL; 
  i INTEGER;
  j INTEGER;
  spaces VARCHAR2(30) := '   '; 
  rowArray t_number := t_number();
BEGIN
  dbms_output.put_line('1'); 
  dbms_output.put(rpad(1,9,' '));
  dbms_output.put(rpad(1,9,' '));
  dbms_output.put_line(''); 
  
  FOR i IN 1 .. N LOOP
    rowArray.extend;
  END LOOP;
  
  rowArray(1) := 1;
  rowArray(2) := 1;    
  
  FOR i IN 3 .. N 
  LOOP
    rowArray(i) := 1;    
    j := i - 1;
    WHILE j > 1 LOOP
      rowArray(j) := rowArray(j) + rowArray(j-1);
      j := j - 1;
    END LOOP;
    FOR j IN 1 .. i LOOP
      dbms_output.put(rpad(rowArray(j),9,' '));
    END LOOP;
    
    dbms_output.put_line(''); 
  END LOOP;
END;
/
```
![第二步](2.png)

## 打印杨辉三角
```sql
set serveroutput on;
call YHTriange(12);
```
![第三步](3.png)
## 实验注意事项

- 完成时间：2023-05-15，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test4目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
