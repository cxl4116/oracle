# 实验3：创建分区表

- 学号：202010414302
- 姓名：陈小龙

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验参考

- 使用sql-developer软件创建表，并导出类似以下的脚本。
- 以下脚本不含orders.customer_name的索引，不含序列设置，仅供参考。

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
--以后再逐年增加新年份的分区
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;
```

这段代码创建了一个名为 "orders" 的表，其中包含了订单信息，表中的每个订单都有一个唯一的订单号（order_id），客户姓名（customer_name）、客户电话（customer_tel）、订单日期（order_date）、负责该订单的员工编号（employee_id）、折扣（discount）和应收账款（trade_receivable）等字段。

表采用了分区技术，按照订单日期（order_date）进行分区，分为三个分区：PARTITION_BEFORE_2016、PARTITION_BEFORE_2020和PARTITION_BEFORE_2021。表的分区策略是按照日期进行分区，每个分区包含该日期之前的订单。对于每个分区，都指定了表空间、PCTFREE、INITRANS等参数，以及存储属性，例如初始大小、下一个大小、最小扩展、最大扩展等。此外，还指定了 NOLOGGING、BUFFER_POOL、NOCOMPRESS、NO INMEMORY 等参数来控制分区的性能和行为。最后，使用 ALTER TABLE 语句为表添加了一个新的分区 partition_before_2022，该分区包含 2022 年之前的订单，并指定了相应的表空间。这意味着在以后的时间里，可以通过类似的 ALTER TABLE 语句，为表添加新的分区来处理新的订单。

这个表的数据将存储在名为"USERS"的表空间中，初始空间占用率为10%，并且不采用压缩和并行处理方式。每个分区的初始化大小为8MB，下一个扩展大小为1MB，并且可以无限扩展，不采用In-Memory技术。

![](./1.png)

- 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```

创建了一个名为"order_details"的表，用于存储订单中的商品详细信息。该表包含了商品详细信息的ID、订单ID、商品ID、商品数量和商品单价等字段。

其中，"id"是该表的主键，并且"order_id"字段是一个外键，引用了"orders"表中的"order_id"字段。这个外键关系被启用，并且在"order_details_fk1"分区键的引导下进行引用分区操作。

这个表的数据将存储在名为"USERS"的表空间中，初始空间占用率为10%，并且不采用压缩和并行处理方式。每个分区的初始化大小为8MB，并且下一个扩展大小为1MB。这个表使用了基于引用分区的方式来进行分区管理，即它的分区方式和"orders"表中的分区方式相同。

![](./2.png)

- 创建序列SEQ1的语句如下

```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```

创建名为 "SEQ1" 的序列的 SQL 语句。该序列具有以下特征：

- MINVALUE：最小值为1。
- MAXVALUE：最大值为999999999。
- INCREMENT BY：每次递增1。
- START WITH：序列的起始值为1。
- CACHE：缓存的序列值数为20。这意味着，当序列的值被请求时，数据库将缓存下一个20个序列值，以提高性能。
- NOORDER：序列值的顺序不保证按顺序返回。
- NOCYCLE：当序列达到最大值时，它不会循环到最小值，而是停止生成序列值。
- NOKEEP：在序列生成期间，如果事务回滚，序列值不会保留。
- NOSCALE：序列的范围将不进行缩放。
- GLOBAL：序列是全局可见的，可由任何用户访问。

执行该语句后，可以通过调用 "SEQ1.NEXTVAL" 获取下一个序列值。

![](./3.png)

- 插入100条orders记录的样例脚本如下：

```sql
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
/

说明：
|| 表示字符连接符号
SEQ1是一个序列对象
```

这段PL/SQL代码是一个简单的循环结构，用于向"orders"表中插入100条记录，每条记录包含一个自动生成的订单ID和订单日期。

循环的初始值为i=0，y=2015，m=1和d=12，然后每次循环i增加1，m增加1。当m大于12时，将其重置为1。将y、m和d的值拼接成一个字符串，然后使用to_date函数将其转换为日期格式。最后使用insert语句向"orders"表中插入一条记录，包含一个自动生成的订单ID和日期。

在循环结束后，使用commit语句提交所有的插入操作，使其生效。这段代码中使用了一个名为"SEQ1"的序列来生成订单ID。

![](./4.png)

联合查询：

```sql
SELECT o.order_id, o.customer_name, od.product_id, od.product_num, od.product_price
FROM orders o
JOIN order_details od
ON o.order_id = od.order_id;
```

这个查询语句中，使用了 "JOIN" 关键字将两个表连接起来，连接条件是 "order_id" 列相等。同时查询的列包括了 "orders" 表中的订单 ID 和顾客姓名，以及 "order_details" 表中的产品 ID、数量和价格。

对于这个查询语句，数据库会首先执行一个 "HASH JOIN" 操作，将 "orders" 表和 "order_details" 表中的数据按照 "order_id" 列进行哈希分区。然后，对于每个分区，使用 "HASH JOIN" 将两个表中相同 "order_id" 值的行进行匹配，得到结果集。最后，根据需要查询的列，执行投影操作，返回结果。

总体来说，这个查询语句的执行计划比较简单，但是如果数据量比较大，可能会对内存和 CPU 资源造成一定的压力。因此，对于大型数据库，通常需要对查询语句和表结构进行优化，以提高查询性能。例如可以建立适当的索引，或者使用更高效的连接算法等。

![](./5.png)

```sql
SELECT o.order_id, o.order_date, o.customer_name, d.product_id, d.product_num, d.product_price
FROM orders o
JOIN order_details d
ON o.order_id = d.order_id
WHERE o.order_date BETWEEN TO_DATE('2021-01-01', 'yyyy-mm-dd') AND TO_DATE('2022-01-01', 'yyyy-mm-dd');
```

该联合查询的执行计划中应该会使用到orders表的分区信息。在查询中使用了日期的范围查询，这会通过分区键快速定位需要查询的分区，从而提高查询效率。同时，order_details表的引用分区会根据orders表的分区信息自动分配到不同的分区中，也能提高查询效率。

![](./6.png)